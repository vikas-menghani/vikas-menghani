import { get } from "svelte/store";
import { appStore } from "$lib/tidy/stores/app.store";
import { BarType, type intervalbar } from "$lib/local/types/intervalbar.type";
import type { Preset } from "$lib/local/types/preset.type";
import {
  SessionBlockType,
  type Session,
  type SessionBlock,
} from "$lib/local/types/session.type";

export function aggregateFocusFromSessions(sessions: Session[]) {
  let focus = 0;
  sessions.forEach((session) => {
    focus += calculateTotalFocusTime(session.blocks);
  });
  return focus;
}

export function getTotalDurationFromPreset(preset: Preset) {
  let bars: intervalbar[] = generateBarsFromPreset(preset);
  return bars.reduce((sum, item) => sum + item.duration, 0);
}

export function generateBarsFromPreset(preset: Preset) {
  let bars: intervalbar[] = [];
  bars = appendPresetBars(bars, preset);
  if (preset.additional && preset.additional.length > 0) {
    preset.additional.forEach((p) => {
      bars = appendPresetBars(bars, p);
    });
  }
  return bars;
}

function appendPresetBars(bars: any, preset: Preset) {
  for (let i = 0; i < preset.rounds; i++) {
    bars = [
      ...bars,
      {
        duration: get(appStore).isDebugMode
          ? preset.duration
          : preset.duration * 60,
        progress: 0,
        type: BarType.INTERVAL,
      },
    ];
    if (
      preset.brek > 0 &&
      !(
        i === preset.rounds - 1 &&
        (preset.additional === undefined || preset.additional.length < 1)
      )
    ) {
      bars = [
        ...bars,
        {
          duration: get(appStore).isDebugMode ? preset.brek : preset.brek * 60,
          progress: 0,
          type: BarType.BREK,
        },
      ];
    }
  }
  return bars;
}

export function calculateTotalFocusTime(blocks: SessionBlock[]) {
  let totalFocusTime = 0;

  for (let i = 0; i < blocks.length - 1; i++) {
    if (blocks[i].type === SessionBlockType.FOCUS) {
      const startTime = blocks[i].start;
      const nextStartTime = blocks[i + 1].start;
      const duration = nextStartTime - startTime;
      totalFocusTime += duration;
    }
  }
  return totalFocusTime / 1000;
}
