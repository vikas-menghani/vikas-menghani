export enum Control {
    START,
    BREAK,
    FINISH,
    RESUME,
    SKIP,
    EXTEND
}